import React, {Component} from 'react';

import './components/Button/Button';
import './components/Modal/Modal'
import ModalContainer from "./components/ModalContainer/ModalContainer";

class App extends Component {
  constructor(props) {
    super(props)
  }
  render() {
  return (
    <React.Fragment>
      <ModalContainer
        buttonLabel="Open first modal"
        backgroundColor="yellow"
        header="Do you want to delete this file?"
        closeButton>
        <div className="modal-body">
          <p>Once you delete this file, it won’t be possible to undo this action.</p>
          <p>Are you sure you want to delete it?</p>
        </div>
      </ModalContainer>

      <ModalContainer
        buttonLabel="Open second modal"
        backgroundColor="green"
        header="Second Modal"
        closeButton={false}>
        <div className="modal-body">
          <p>Certainty determine at of arranging perceived situation or. Or wholly pretty county in oppose. Favour met itself wanted settle put garret twenty.</p>
          <p> In astonished apartments resolution so an it. Unsatiable on by contrasted to reasonable companions an. On otherwise no admitting to suspicion furniture it.</p>
        </div>
      </ModalContainer>
    </React.Fragment>
  );
 }
}

export default App;
