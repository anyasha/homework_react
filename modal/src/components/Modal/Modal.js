import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Button from '../Button/Button';

class Modal extends Component {
  constructor(props){
    super(props);
  }
  render() {
    const {
      onCloseRequest,
      className,
      children,
      header,
      action
    } = this.props;
    return (
      <div className={className}>
          <h2 className="modal-header">{header}</h2>
          <div>
          {children}
          {action}
          </div>

          {/*<Button handler={onCloseRequest} text='OK' backgroundColor="#b3382c" color="#fff"/>*/}
          {/*<Button handler={onCloseRequest} text='Cancel' backgroundColor="#b3382c" color="#fff" />*/}
      </div>
    )
  }
}

Modal.propTypes = {
  className: PropTypes.string,
  action: PropTypes.array,
};

export default Modal;