import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './Button.scss';

class Button extends Component{
  constructor(props) {
    super(props)
  }
  render() {
    const {
      backgroundColor,
      color,
      text,
      handler,
    } = this.props;
    return (
      <button
        onClick={handler}
        className="button"
        style={{backgroundColor, color}}>
          {text}
      </button>
    )
  }
}

Button.propTypes = {
  backgroundColor: PropTypes.string,
  color: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  backgroundColor: '#b3382c',
  color: 'white',
  className: 'button',
};

export default Button;