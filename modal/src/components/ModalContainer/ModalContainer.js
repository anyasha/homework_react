import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes} from '@fortawesome/free-solid-svg-icons'


import Button from '../Button/Button'
import Modal from "../Modal/Modal";
import '../Modal/Modal.scss';

class ModalContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
    }
  }
  openModal = () => {
    this.setState({showModal: true});
  };
  closeModal = () => {
    this.setState({showModal: false});
  };

  render() {
    const {
      buttonLabel,
      backgroundColor,
      children,
      header,
      closeButton,
    } = this.props;
    const { showModal } = this.state;
    const action = [
      <Button text='Ok' onClick={this.closeModal} key="0"/>,
      <Button text='Cancel' onClick={this.closeModal} key="1"/>
      ];
    return(
      <React.Fragment>
        <Button handler={this.openModal} text={buttonLabel} backgroundColor={backgroundColor} color='black'/>
        {showModal && (
            <div onClick={this.closeModal} className="modal-overlay">
              <Modal
                action={action}
                className="modal-content"
                 header = {header}
                 onCloseRequest={() => this.closeModal()} >
                { closeButton ?
                  <FontAwesomeIcon icon={faTimes} style={{position: 'absolute', right: '15px', top: '15px', color: 'white', cursor: 'pointer'}} onClick={this.closeModal}/>
                  : null}
                {children}
              </Modal>
            </div>
        )}
</React.Fragment>
    )
  }
}
ModalContainer.propTypes = {
  buttonLabel: PropTypes.string,
  backgroundColor: PropTypes.string,
  children: PropTypes.object,
  header: PropTypes.string,
  closeButton: PropTypes.bool,
};

export default ModalContainer;