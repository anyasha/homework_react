const initialState = {
  modal: true,
};

export default function modalState(state = initialState, action) {
  switch (action.type) {
    case 'TOGGLE_MODAL':
      return {...state, modal: action.payload.modal}
  }
}