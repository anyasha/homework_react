import {combineReducers} from 'redux';
import modalState from './modalState'

const rootReducer = combineReducers(
  {
    modalState
  }
);

export default rootReducer;
