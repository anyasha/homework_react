import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './public/items.json'
import {createStore} from "redux";
import {Provider} from 'react-redux';
import rootReducer from './reducers/index'

// class App extends Component {
//   constructor(props) {
//     super(props)
// }
//   render() {
//     return (
//
//     )
// }
// }
const store = createStore(rootReducer);
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, document.getElementById('root'));

