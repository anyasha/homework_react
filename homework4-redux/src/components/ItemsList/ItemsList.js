import React,{Component} from 'react';
import Item from "../Item/Item";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";


class ItemList extends Component {
  constructor(props) {
    super(props);
    // const [items] = this.props;
    this.state = {items : [],
      showModal: false
    };
    console.log(this.state);
  }



  componentDidMount() {
    this.setState({
      items: this.props.items,
    })
  }


  render() {
    const {items} = this.state;
    console.log(items);
    return (
      <div>
        <ul>
          {items.map((item,index) => (
            <li key={item+index}>
              <Item title={item.title} price={item.price} url={item.url} article={item.art} color={item.color}/>
            </li>
          ))}

        </ul>
      </div>
    )
  }
}

export default ItemList
