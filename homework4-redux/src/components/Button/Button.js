import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './Button.scss'

class Button extends Component{
  constructor(props) {
    super(props)
  }
  render() {
    const { backgroundColor,
    color,
    text,
    handler, } = this.props;
    // console.log(this.props);
    return (
      <button onClick={handler} className="button" style={{backgroundColor, color}}>{text}</button>
    )
  }

}

export default Button;