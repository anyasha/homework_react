import React,{Component} from 'react';
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
// import '../Modal/Modal.scss';
// import './CardsItem.css'

class CardsItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      isFavorite: false
    };
  }

  openModal = () => {
    this.setState({showModal: true});
    // console.log(this.state);
    // console.log('work');
  };

  closeModal = () => {
    this.setState({showModal: false});
    // console.log('close');
  };

  addFavorite = () => {
    this.setState({isFavorite: !this.state.isFavorite});

  };


  render() {
    const {title, color, url, article, price, closeButton} = this.props;
    const {showModal, isFavorite} = this.state;
    console.log(this.state);
    const style = { color: isFavorite ? 'yellow' : 'black'};
    return (
      <div>

        <h4>{title}</h4>
        <p>{color}</p>
        <p>{article}</p>
        <p>{price}</p>
        <p onClick={this.addFavorite.bind(this)} style={style}> STAR</p>
        <img src={url} alt=""/>
        <Button text='Add card' handler={this.openModal} />
        {showModal && (
          <div>
            <div onClick={this.closeModal} className='back-drop' >

              <Modal className="modal-content"  onCloseRequest={() => this.closeModal()} >
                { closeButton ? <Button text="x"/> : null}
                <p>Are you sure to add this item</p>
              </Modal>
            </div>
          </div>
        )}
      </div>
    )
  }
}

export default CardsItem
