import React, {Component} from 'react';
import propTypes from 'prop-types';

import Button from '../Button/Button'

class Modal extends Component {
  constructor(props){
    super(props);

  }

  render() {
    const {
      onCloseRequest,
      className,
      children,
      header,
      closeButton
    } = this.props;
    return (
      <div className={className}>
        <div><div>
          <h2>{header}</h2>
          {/*{ {closeButton} ? <Button text="X"/> : null}*/}
          {children}</div>
          <Button handler={onCloseRequest} text='OK' backgroundColor="#b3382c" color="#fff"/>
          <Button handler={onCloseRequest} text='Cancel' backgroundColor="#b3382c" color="#fff" />
          </div>

        {/*<div>*/}
        {/*  <p>{header}</p>*/}
        {/*  /!*{ {closeButton} ? <Button text="X"/> : null}*!/*/}
        {/*</div>*/}
        {/*<Button handler={onCloseRequest} text='OK' />*/}
        {/*<Button handler={onCloseRequest} text='Close' />*/}

      </div>
    )
  }
}

export default Modal;