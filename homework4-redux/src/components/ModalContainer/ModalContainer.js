import React, {Component} from 'react';
import propTypes from 'prop-types';

import Button from '../Button/Button'
import Modal from "../Modal/Modal";
import '../Modal/Modal.css';

class ModalContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
    }
  }
  openModal = () => {
    this.setState({showModal: true});
    // console.log(this.state);
    console.log('work');
  }
  closeModal = () => {
    this.setState({showModal: false});
    console.log('close');
  }

  render() {
    const {
      buttonLabel,
      backgroundColor,
      children,
      header,
      closeButton
    } = this.props;
    const { showModal } = this.state;
    console.log(this.props);
    return(
<div>
      <Button handler={this.openModal} text={buttonLabel} backgroundColor={backgroundColor} color='black'/>

    {showModal && (
      <div>
        <div onClick={this.closeModal} className="back-drop">

          <Modal className="modal-content" header = {header} onCloseRequest={() => this.closeModal()} >
            { closeButton ? <Button text="x"/> : null}
            {children}
          </Modal>
        </div>
      </div>
    )}
</div>
    )
  }
}

export default ModalContainer;