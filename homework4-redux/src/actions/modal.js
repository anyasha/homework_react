export function toggleModal(modal) {
  return {
    type: 'TOGGLE_MODAL',
    payload: {
      modal
    }
  }
}