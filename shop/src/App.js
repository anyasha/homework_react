import React, {Component} from 'react';

import './components/Button/Button';
import './components/Modal/Modal'
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal"
import items from "./public/items.json";
import ModalContainer from "./components/ModalContainer/ModalContainer";
import ItemList from "./components/ItemsList/ItemsList";

class App extends Component {
  constructor(props) {
    super(props)
  }
  render() {
  return (
    <div>
      {/*<ModalContainer*/}
      {/*  buttonLabel="Open first modal"*/}
      {/*  backgroundColor="yellow"*/}
      {/*  header="Do you want to delete this file?"*/}
      {/*  closeButton>*/}
      {/*  <div>*/}
      {/*    <p>Once you delete this file, it won’t be possible to undo this action.</p>*/}
      {/*    <p>Are you sure you want to delete it?</p>*/}
      {/*  </div>*/}
      {/*</ModalContainer>*/}

      {/*<ModalContainer*/}
      {/*  buttonLabel="Open second modal"*/}
      {/*  backgroundColor="green"*/}
      {/*  header="Second Modal"*/}
      {/*  closeButton={false}><div>lorem</div></ModalContainer>*/}
     <ItemList items={items} />
    </div>
  );

  }
}

// function openFirstModal() {
//   // console.log('hello');
// }
// function openSecondModal() {
//   // console.log('bay');
// }


export default App;
