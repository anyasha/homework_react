import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './public/items.json'


// const data = function getProducts() {
  const data = fetch('./items.json')
    .then(response => response.json())

    .then(data=>({...data})
    )

// }
console.log(data);

ReactDOM.render(<App />, document.getElementById('root'));

