import React,{Component} from 'react';
import Item from "../Item/Item";
import './ItemList.scss'
import Modal from "../Modal/Modal";
import Button from "../Button/Button";


class ItemList extends Component {
  constructor(props) {
    super(props);
    this.state = {items : [],
      isFavorite: false,
      favorite: JSON.parse(localStorage.getItem('item')) || [],
      showModal: false
    };
    this.updateState = this.updateState.bind(this);
  }

  componentDidMount() {
    fetch('./items.json')
      .then(response => response.json())
      // .then(data => this.setState({...data}))
      .then(data => this.setState({
        items: data.products
      }));
    // this.setState({
    //   items: this.props.items,
    // })
  }
  addFavorite = () => {
    this.setState({isFavorite: !this.state.isFavorite});

  };


updateState(who) {

  // (this.state.favorite.forEach((item) =>
  // item.article === who.article)) ?
   this.setState({favorite: this.state.favorite.concat(who)}, () => {
    localStorage.setItem('item', JSON.stringify(this.state.favorite));
    })
// :
//   alert('hello')


  }

  render() {
    const {items} = this.state;
    const {favorite} = this.state;
    console.log(items);
    return (
      <div className="cards-desk">
          {items.map((item,index) => (
            <React.Fragment>
              <Item
                key={item+index}
                title={item.title}
                price={item.price}
                url={item.url}
                article={item.art}
                color={item.color}
                update={this.updateState}/>
            </React.Fragment>
          ))}
      </div>
    )
  }
}

export default ItemList
