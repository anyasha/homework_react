import React,{Component} from 'react';
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar} from '@fortawesome/free-solid-svg-icons'
// import '../Modal/Modal.scss';
import './Item.scss'

class CardsItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      isFavorite: false
    };
  }

  openModal = () => {
    this.setState({showModal: true});
    console.log(this.state);
  };

  closeModal = () => {
    this.setState({showModal: false});
  };

  addFavorite = () => {
    this.setState({isFavorite: !this.state.isFavorite});

  };


  render() {
    const {title, color, url, article, price, closeButton} = this.props;
    const {showModal, isFavorite} = this.state;
    console.log(this.state);
    const style = { color: isFavorite ? 'yellow' : 'black'};
    return (
      <div className="card">
        <div className="card__body">
          <div className="half">
            <div className="featured_text">
              <h1>{title}</h1>
              <p className="sub">{article}</p>
              <p className="price">{price}</p>
            </div>
            <div className="image">
              <img src={url} alt="" style={{width: 200+'px'}}/>
            </div>
          </div>
          <div className="half">
            <div className="description">
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero voluptatem nam pariatur voluptate perferendis, asperiores aspernatur! Porro similique consequatur, nobis soluta minima, quasi laboriosam hic cupiditate perferendis esse numquam magni.</p>
            </div>
            <span className="stock"><i className="fa fa-pen"></i> In stock</span>
            <div className="reviews">
              <FontAwesomeIcon
                onClick={() => this.props.update(this.props) }
                // onClick={this.addFavorite}
                style={style}
                className="stars"
                icon={faStar} />

              <span>(64 reviews)</span>
            </div>
          </div>
        </div>
      <div className="card__footer">
        <div className="recommend">
          <p>Recommended by</p>
          <h3>Andrew Palmer</h3>
        </div>
        <div className="action">
          <Button text='Add to card' handler={this.openModal} />
          {showModal && (
            //<div>
              //<div onClick={this.closeModal} className='back-drop' >

                <Modal className="modal-content"  onCloseRequest={() => this.closeModal()} >
                  { closeButton ? <Button text="x"/> : null}
                  <p>Are you sure to add this item</p>
                </Modal>
            //   </div>
            // </div>
          )}
          {/*<button type="button">Add to cart</button>*/}
        </div>
      </div>
      </div>
        )
  }
}


      {/*<div className="card-wrapper">*/}

      {/*  <h4>{title}</h4>*/}
      {/*  <p>{color}</p>*/}
      {/*  <p>{article}</p>*/}
      {/*  <p>{price}</p>*/}
      {/*  <p onClick={this.addFavorite.bind(this)} style={style}> STAR</p>*/}
      {/*  <img src={url} alt="" style={{width: 200+'px'}}/>*/}
      {/*  <Button text='Add card' handler={this.openModal} />*/}
      {/*  {showModal && (*/}
      {/*    <div>*/}
      {/*      <div onClick={this.closeModal} className='back-drop' >*/}

      {/*        <Modal className="modal-content"  onCloseRequest={() => this.closeModal()} >*/}
      {/*          { closeButton ? <Button text="x"/> : null}*/}
      {/*          <p>Are you sure to add this item</p>*/}
      {/*        </Modal>*/}
      {/*      </div>*/}
      {/*    </div>*/}
      {/*  )}*/}
      {/*</div>*/}


export default CardsItem
